﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Cities
{
    class City
    {
        public string Name;
        public double PopulationMillions;

        public override string ToString()
        {
            return string.Format("City: {0} with {1} mil. population", Name, PopulationMillions);
        }
    }

    class BetterCity
    {
        public string Name;
    }
    class Program
    {

        static public List<City> CitiesList = new List<City>();
        static void Main(string[] args)
        {
            try
            {
                City c1 = new City();
                c1.Name = "Montreal";
                c1.PopulationMillions = 2.5;
                Console.WriteLine(c1);

                City c2 = new City { Name = "Toronto", PopulationMillions = 4.5 }; // object initializer, same as above but shorter

                BetterCity bc1 = new BetterCity();
                bc1.Name = "Vancouver";

                CitiesList.Add(new City() { Name = "New York" });
                CitiesList.Add(new City() { Name = "LA" });
                CitiesList.Add(new City() { Name = "London" });

                foreach (City c in CitiesList)
                {
                    Console.WriteLine("City is" + c);
                }

            } finally
            {
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }
            
        } 

    }
}
