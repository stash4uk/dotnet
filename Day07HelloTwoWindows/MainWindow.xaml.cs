﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07HelloTwoWindows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonSayHello_Click(object sender, RoutedEventArgs e)
        {
            int ageVal = 0;
            double randVal = 0;
            
            string name = tbName.Text;
            HelloDialog helloDialog = new HelloDialog(name);
            helloDialog.Owner = this;
            helloDialog.AssignResult += (a, r) => { ageVal = a; randVal = r; };

            bool? result = helloDialog.ShowDialog();
            Console.WriteLine("Result is: " + result);
            if (result == true)
            { // OK was clicked
                lblResult.Content = ("Age: {0}, Rand: {1}" + ageVal, randVal);
            }
        }
    }
}
