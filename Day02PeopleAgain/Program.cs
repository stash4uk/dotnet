﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{

	public class InvalidParameterException : Exception
	{
		public InvalidParameterException(string msg) : base(msg) { }
	}

	public class Person
	{
		private string _name;
		public string Name // 1-50 characters, no semicolons
		{
			get
			{
				return _name;
			}
			set
			{
				if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
				{
					throw new InvalidParameterException("Name must be 1-50 characters long, no semicolons");
				}
			}
		}

		private int _age;
		public int Age // 0-150
		{
			get
			{
				return _age;
			}
			set
			{
				if (value < 0 || value > 150)
				{
					throw new InvalidParameterException("Age must be 0-150");
				}
			}
		}
		public Person(string dataLine)
		{
			string[] data = dataLine.Split(';');
			/* // can't do these verifications when constructor called from the constructor of a child class
			if (data.Length != 3)
            {
				throw new InvalidParameterException("Line has invalid number for fields:\n" + dataLine);
            }
			if (data[0] != "Person")
            {
				throw new InvalidParameterException("Line does not define Person:\n" + dataLine);
			} */
			Name = data[1];
			int age;
			if (int.TryParse(data[2], out age))
			{
				throw new InvalidParameterException("Line age must be an integer value:\n" + dataLine);
			}
			Age = age;
		}
		public Person(string name, int age)
		{
			Name = name;
			Age = age;
		}

		public override string ToString()
		{
			return string.Format("Person {0} is {1} y/o", Name, Age);
		}

		public virtual string ToDataString()
		{
			return string.Format("Person;{1};{2}", Name, Age);
		}
	}

	public class Teacher : Person
	{
		private string _subject;
		public string Subject // 1-50 characters, no semicolons
		{
			get
			{
				return _subject;
			}
			set
			{
				if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
				{
					throw new InvalidParameterException("Subject must be 1-50 characters long, no semicolons");
				}
			}
		}

		private int _yoe;
		public int YearsOfExperience // 0-100
        {
			get
			{
				return _yoe;
			}
			set
			{
				if (value < 0 || value > 100)
				{
					throw new InvalidParameterException("Years of experience must be 0-100");
				}
			}
		}

		public Teacher(string dataLine) : base(dataLine)
		{
			string[] data = dataLine.Split(';');
			Subject = data[3];
			int yoe;
			if (int.TryParse(data[4], out yoe))
			{
				throw new InvalidParameterException("Years of experience must be an integer value:\n" + dataLine);
			}
			YearsOfExperience = yoe;
		}
		public Teacher(string name, int age, string subject, int yoe) : base(name, age)
		{
			Subject = subject;
			YearsOfExperience = yoe;
		}
		public override string ToString()
		{
			return string.Format("Teacher {0} is {1} y/o, teaches {2} since {3} years", Name, Age, Subject, YearsOfExperience);
		}
		public override string ToDataString()
		{
			return string.Format("Teacher;{1};{2};{3};{4}", Name, Age, Subject, YearsOfExperience);
		}

	}

	public class Student : Person
	{
		private string _program;
		public string Program // 1-50 characters, no semicolons
        {
			get
			{
				return _program;
			}
			set
			{
				if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
				{
					throw new InvalidParameterException("Program must be 1-50 characters long, no semicolons");
				}
			}
		}

		private double _gpa;
		public double GPA // 0-4.3
        {
			get
			{
				return _gpa;
			}
			set
			{
				if (value < 0 || value > 4.3)
				{
					throw new InvalidParameterException("GPA must be 0-4.3");
				}
			}
		}

		public Student(string dataLine) : base(dataLine)
		{
			string[] data = dataLine.Split(';');
			Program = data[3];
			int gpa;
			if (int.TryParse(data[4], out gpa))
			{
				throw new InvalidParameterException("Years of experience must be an integer value:\n" + dataLine);
			}
			GPA = gpa;
		}
		public Student(string name, int age, string program, double gpa) : base(name, age)
		{
			Program = program;
			GPA = gpa;
		}
		public override string ToString()
		{
			return string.Format("Student {0} is {1} y/o, studies {2} with {3} GPA", Name, Age, Program, GPA);
		}
		public override string ToDataString()
		{
			return ""; // FIXME
		}

	}

	class Program
	{
		public const string FilePath = @"..\..\people.txt";
		static List<Person> peopleList = new List<Person>();
        private static string dataLine;

        static void ReadAllPeopleFromFile()
		{
			Person p = new Person(dataLine);
			peopleList.Add(p);
			Teacher t = new Teacher(dataLine);
			peopleList.Add(t);
			Student s = new Student(dataLine);
			peopleList.Add(s);
		}

		static void Main(string[] args)
		{
			ReadAllPeopleFromFile();
			Console.ReadLine();
		}
	}
}
