﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2Travel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        static List<Trip> trips = new List<Trip>();
        SaveFileDialog sfd = new SaveFileDialog();

        private void ClearInputs()
        {
            tbDestination.Text = "";
            tbName.Text = "";
            tbPassNo.Text = "";
            dpDeptDate.Text = "";
            dpRetDate.Text = "";
        }

        private void Button_Click_Add(object sender, RoutedEventArgs e)
        {
            try
            {
                string destination = tbDestination.Text;
                string name = tbName.Text;
                string passNo = tbPassNo.Text;
                DateTime deptDate = (DateTime)(dpDeptDate.SelectedDate ?? null);
                DateTime retDate = (DateTime)(dpRetDate.SelectedDate ?? null);

                Trip t = new Trip(destination, name, passNo, deptDate, retDate);
                trips.Add(t);
                lvTrips.ItemsSource = trips;
            }
            catch (InvalidParameterException ex)
            {
                MessageBox.Show("No date input" + ex.Message);
            }

            
            
            
        }

        private void Button_Click_Edit(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_Delete(object sender, RoutedEventArgs e)
        {
            if (lvTrips.SelectedIndex != -1)
            {
                trips.RemoveAt(lvTrips.SelectedIndex);
                lvTrips.Items.Refresh();
                ClearInputs();
            }
        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            sfd.FileName = "";
            sfd.Filter = "Text Documents *.txt|All Files *.*";
            sfd.DefaultExt = ".txt";
            string data = "";
            if (sfd.ShowDialog() == true)
            {
                foreach (Trip t in lvTrips.SelectedItems)
                {
                    data += t.ToDataString() + "\n";
                }

                File.WriteAllText(sfd.FileName, data);
            }
        }
    }

    public class Trip
    {
        public Trip(string destination, string name, string passNo, 
            DateTime deptDate, DateTime retDate)
        {
            Destination = destination;
            Name = name;
            PassNo = passNo;
            DeptDate = deptDate;
            RetDate = retDate;
        }

        private string _destination;
        private string _name;
        private string _passNo;
        private DateTime _deptDate;
        private DateTime _retDate;

        public string Destination
        {
            get
            {
                return _destination;
            }
            set
            {
                if (value.Length < 2 || value.Length > 30 || value.Contains(";"))
                {
                    throw new ArgumentException("Destination must be between " +
                        "2 and 30 characters long. No semicolons allowed.");
                }
                _destination = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 30 || value.Contains(";"))
                {
                    throw new ArgumentException("Name must be between " +
                        "2 and 30 characters long. No semicolons allowed.");
                }
                _name = value;
            }
        }

        public string PassNo
        {
            get
            {
                return _passNo;
            }
            set
            {
                if (!Regex.Match(value, @"[A-Z]{2}[0-9]{6}").Success)
                {
                    throw new InvalidParameterException("Passport No " +
                        "must be of the AB123456 format");
                }
                _passNo = value;
            }
        }

        public DateTime DeptDate
        {
            get
            {
                return _deptDate;
            }
            set
            {
                _deptDate = value;
            }
        }

        public DateTime RetDate
        {
            get
            {
                return _retDate;
            }
            set
            {
                /*if (RetDate.SelectedDate > DeptDate.SelectedDate)
                {
                    throw new InvalidParameterException("Return date must be" +
                        "later than Departure date.");
                }*/
                _retDate = value;
            }
        }

        public Trip(string dataLine)
        {
            string[] data = dataLine.Split(';');

            if (data.Length != 5)
            {
                throw new InvalidParameterException("Line has invalid number for fields:\n" + dataLine);
            }

            Destination = data[0];
            Name = data[1];
            PassNo = data[2];
            try
            {
            DeptDate = DateTime.Parse(data[3]);
            RetDate = DateTime.Parse(data[4]);
            } 
            catch
            {
                throw new InvalidParameterException("Date parsing error:\n" + dataLine);
            }
        }
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4}", 
                Destination, Name, PassNo, DeptDate, RetDate);
        }
        public string ToDataString()
        {
            return string.Format("{0};{1};{2};{3};{4}", Destination, Name, PassNo, DeptDate, RetDate);
        }
    }

    public class InvalidParameterException : Exception
    {
        public InvalidParameterException(string msg) : base(msg) { }
    }
}
