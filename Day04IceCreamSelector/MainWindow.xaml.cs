﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day04IceCreamSelector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<Flavour> items = new List<Flavour>();
            items.Add(new Flavour() { Name = "Vanilla"});
            items.Add(new Flavour() { Name = "Chocolate"});
            items.Add(new Flavour() { Name = "Strawberry"});
            items.Add(new Flavour() { Name = "Peach"});
            lstFlavours.ItemsSource = items;

            //List<string> selected = new List<string>();
        }

        private void Button_Click_Add(object sender, RoutedEventArgs e)
        {
            if (lstFlavours.SelectedItem == null)
            {
                MessageBox.Show("No flavour selected");
            }
            
                //lstSelected.ItemsSource = lstFlavours.SelectedItems;
            foreach (var item in lstFlavours.SelectedItems)
            {
                lstSelected.Items.Add(item);
            }
            
        }

        private void Button_Click_Delete(object sender, RoutedEventArgs e)
        {
            if (lstSelected.SelectedItem == null)
            {
                MessageBox.Show("No flavour selected", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            
            do
            {
                lstSelected.Items.Remove(lstSelected.SelectedItem);
            }
            while (lstSelected.SelectedItems.Count > 0);
        }

        private void Button_Click_ClearAll(object sender, RoutedEventArgs e)
        {

            if (lstSelected.Items.Count == 0)
            {
                MessageBox.Show("No flavour selected", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                var confirmResult = MessageBox.Show("Delete selected items?", "Confirm Delete!",
                MessageBoxButton.OKCancel, MessageBoxImage.Question);

                if (confirmResult == MessageBoxResult.OK)
                {
                    lstSelected.Items.Clear();
                }
            }
        }
    }

    public class Flavour
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
