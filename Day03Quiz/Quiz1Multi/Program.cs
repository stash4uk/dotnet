﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz1Multi
{
    public class InvalidParameterException : Exception
    {
        public InvalidParameterException(string msg) : base(msg) { }
    }

    public class Airport
    {
        public Airport(string code, string city, double lat, double lng, int elevM) 
        {
            Code = code;
            City = city;
            Latitude = lat;
            Longitude = lng;
            ElevationMeters = elevM;
        }
        public Airport(string dataLine) 
        {
            string[] data = dataLine.Split(';');
            
			if (data.Length != 5)
            {
				throw new InvalidParameterException("Line has invalid number for fields:\n" + dataLine);
            }

            Code = data[0];
            City = data[1];

            double lat;
            if (double.TryParse(data[2], out lat))
            {
                throw new InvalidParameterException("Line age must be an integer value:\n" + dataLine);
            }
            Latitude = lat;

            double lng;
            if (double.TryParse(data[3], out lng))
            {
                throw new InvalidParameterException("Line age must be an integer value:\n" + dataLine);
            }
            Longitude = lng;

            int elevM;
            if (int.TryParse(data[4], out elevM))
            {
                throw new InvalidParameterException("Line age must be an integer value:\n" + dataLine);
            }
            ElevationMeters = elevM;
        }

        private string _code;
        string Code // exactly 3 uppercase letters, use regexp
        {
            get
            {
                return _code;
            }
            set
            {
                if (!Regex.Match(value, "^[A-Z]{3}$").Success)
                {
                    throw new InvalidParameterException("Code must be 3 characters long, upper case");
                }
                _code = value;
            }
        }

        private string _city;
        string City // 1-50 characters, made up of uppercase and lowercase letters, digits, and .,- characters
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 1 || value.Length > 50 ||!Regex.Match(value, @"[A-Za-z0-9\\.\\,-]+").Success || value.Contains(';'))
                {
                    throw new InvalidParameterException("City must be 1-50 characters, " +
                        "made up of uppercase and lowercase letters, digits, and .,- characters");
                }
                _city = value;
            }
        }

        private double _lat;
        double Latitude // -90 to 90
        {
            get
            {
                return _lat;
            }
            set
            {
                if (value < -90 || value > 90)
                {
                    throw new InvalidParameterException("Latitude must be -90 to 90");
                }
                _lat = value;
            }
        }

        private double _lng;
        double Longitude // -180 to 180
        {
            get
            {
                return _lng;
            }
            set
            {
                if (value < -180 || value > 180)
                {
                    throw new InvalidParameterException("Latitude must be -180 to 180");
                }
                _lng = value;
            }
        }

        private int _elevM;
        int ElevationMeters //-1000 to 10000
        {
            get
            {
                return _elevM;
            }
            set
            {
                if (value < -1000 || value > 10000)
                {
                    throw new InvalidParameterException("Elevation must be -1000 to 10000 meters");
                }
                _elevM = value;
            }
        }
	public override string ToString() 
        {
            return string.Format("{0};{1};{2};{3};{4}", Code, City, Latitude, Longitude, ElevationMeters);
        }
        public string ToDataString()
        {
            return string.Format("{0};{1};{2};{3};{4}", Code, City, Latitude, Longitude, ElevationMeters);
        }
    }

    class Program
    {
        public const string FilePath = @"..\..\data.txt";
        static List<string> output = new List<string>();
        static List<Airport> airportList = new List<Airport>();

        private static int GetMenuChoice()
        {
            while (true)
            {
                Console.Write(
@"1. Add Airport
2. List all airports
3. Find nearest airport by code
4. Find airport's elevation standard deviation
5. Change log delegates
0. Exit
Enter your choice: "
                    );
                string choiceStr = Console.ReadLine();
                int choice;
                if (!int.TryParse(choiceStr, out choice) || choice < 0 || choice > 5)
                {
                    Console.WriteLine("Error: Enter an integer value between 0 and 5");
                    continue;
                }
                return choice;
            }
        }

        static void ReadDataFromFile()
        {
            List<string> lines = File.ReadAllLines(FilePath).ToList();

            foreach (var line in lines)
            {
                string[] records = line.Split(';');

                String code = records[0];
                String city = records[1];
                double lat = Convert.ToDouble(records[2]);
                double lng = Convert.ToDouble(records[3]);
                int elevM = Convert.ToInt32(records[4]);
                Airport a = new Airport(code, city, lat, lng, elevM);
                airportList.Add(a);
                Console.WriteLine(a.ToString());
            }
            Console.WriteLine("\n Data read from file.");
        }

        static void AddAirport()
        {
            Console.Write("Enter code: ");
            string code = Console.ReadLine();
            Console.Write("Enter city: ");
            string city = Console.ReadLine();
            Console.Write("Enter latitude: ");
            string latStr = Console.ReadLine();
            double lat;
            if (!double.TryParse(latStr, out lat))
            {
                Console.WriteLine("Error: Enter a decimal point value for latitude.");
                return;
            }
            Console.Write("Enter longitude: ");
            string lngStr = Console.ReadLine();
            double lng;
            if (!double.TryParse(lngStr, out lng))
            {
                Console.WriteLine("Error: Enter a decimal point value for longitude.");
                return;
            }
            Console.Write("Enter elevation in meters: ");
            string elevMStr = Console.ReadLine();
            int elevM;
            if (!int.TryParse(elevMStr, out elevM))
            {
                Console.WriteLine("Error: Enter an integer value for elevation.");
                return;
            }

            try
            {
                Airport airport = new Airport(code, city, lat, lng, elevM);
                airportList.Add(airport);

                Console.WriteLine("Airport added.");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        static void ListAllAirports()
        {

        }

        static void FindNearestAirport()
        {

        }

        static void FindAirportElevation()
        {

        }

        static void ChangeLogDelegates()
        {

        }

        static void WriteDataToFile()
        {
            foreach (Airport airport in airportList)
            {
                //output.Add(airport.ToDataString);
            }
            File.WriteAllLines(FilePath, output);
        }
        static void Main(string[] args)
        {
            ReadDataFromFile();
            Console.WriteLine();
            while (true)
            {
                int choice = GetMenuChoice();
                switch (choice)
                {
                    case 1:
                        AddAirport();
                        break;
                    case 2:
                        ListAllAirports();
                        break;
                    case 3:
                        FindNearestAirport();
                        break;
                    case 4:
                        FindAirportElevation();
                        break;
                    case 5:
                        FindAirportElevation();
                        break;
                    case 0:
                        WriteDataToFile();
                        Console.WriteLine("Data saved. Program will stop.");
                        return;
                    default:
                        Console.WriteLine("Error: Invalid choice.");
                        break;

                }

                Console.ReadLine();
            }
        }
    }
}
