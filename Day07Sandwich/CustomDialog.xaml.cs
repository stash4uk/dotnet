﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day07Sandwich
{
    /// <summary>
    /// Interaction logic for CustomDialog.xaml
    /// </summary>
    public partial class CustomDialog : Window
    {
        public event Action<string, string, string> AssignResult;
        
        public CustomDialog()
        {
            InitializeComponent();
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            string bread = cbBread.Text;
            List<string> vegList = new List<string>();
            if (cbLettuce.IsChecked == true)
            {
                vegList.Add("Lettuce");
            }
            if (cbTomatoes.IsChecked == true)
            {
                vegList.Add("Tomatoes");
            }
            if (cbCucubmers.IsChecked == true)
            {
                vegList.Add("Cucumber");
            }

            string veggies = string.Join(", ", vegList);

            string meat = "";
            if (rbChicken.IsChecked == true)
            {
                meat = "Chicken";
            }
            else if (rbTurkey.IsChecked == true)
            {
                meat = "Turkey";
            }
            else if (rbTofu.IsChecked == true)
            {
                meat = "Tofu";
            }
            else
            {

            }

            AssignResult?.Invoke(bread, veggies, meat);
            DialogResult = true;
        }
    }
}
