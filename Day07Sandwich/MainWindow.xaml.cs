﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07Sandwich
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string breadVal, vegVal, meatVal;
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonMakeSandwich_Click(object sender, RoutedEventArgs e)
        {

            CustomDialog custDig = new CustomDialog();
            // show it as a modal dialog
            custDig.Owner = this;
            custDig.AssignResult += CustDig_AssignResult;
            bool? result = custDig.ShowDialog();
            
        }

        private void CustDig_AssignResult(string bread, string veg, string meat)
        {
            lblBread.Content = bread;
            lblVeggies.Content = veg;
            lblMeats.Content = meat;
        }
    }
}
