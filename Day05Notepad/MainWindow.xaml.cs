﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;

namespace Day05Notepad
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        OpenFileDialog ofd = new OpenFileDialog();
        SaveFileDialog sfd = new SaveFileDialog();


        void SetWindowTitle(string fileName)
        {
            Title = string.Format("{0}", System.IO.Path.GetFileName(fileName));
        }
        public void SaveChanges()
        {
            sfd.FileName = "";
            sfd.Filter = "Text Documents *.txt|All Files *.*";
            sfd.DefaultExt = ".txt";
            if (sfd.ShowDialog() == true)
            {
                File.WriteAllText(sfd.FileName, tbCenter.Text);
            }
        }

        private void MenuItem_Click_OpenFile(object sender, RoutedEventArgs e)
        {
            ofd.DefaultExt = ".txt";
            ofd.Filter = "Text Document |*.txt";
            if (ofd.ShowDialog() == true)
            {
                string fileName = ofd.FileName;
                this.SetWindowTitle(fileName);
                tbCenter.Text = File.ReadAllText(fileName);
                tbBottom.Text = fileName;
            }
        }

        private void MenuItem_Click_SaveAs(object sender, RoutedEventArgs e)
        {
            SaveChanges();
        }

        private void MenuItem_Click_Exit(object sender, RoutedEventArgs e)
        {
            if (tbCenter.Text != null)
            {
                var confirmResult = MessageBox.Show("Save changes?", "Confirm Exit",
                MessageBoxButton.OKCancel, MessageBoxImage.Question);

                if (confirmResult == MessageBoxResult.OK)
                {
                    SaveChanges();
                    Environment.Exit(0);
                }
            }
        }
    }
}
