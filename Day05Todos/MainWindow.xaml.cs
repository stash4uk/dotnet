﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day05Todos
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        List<Todo> TaskList = new List<Todo>();

        private void Button_Click_Add(object sender, RoutedEventArgs e)
        {
            //TaskList.Add(tbDueDate);
            lvTasks.ItemsSource = TaskList;
            lvTasks.Items.Refresh();
        }
    }

    public class InvalidParameterException : Exception
    {
    public InvalidParameterException(string msg) : base(msg) { }
    }

public class Todo
    {
        public Todo(string task, int difficulty, string dueDate, StatusEnum status)
        {
            Task = task;
            Difficulty = difficulty;
            DueDate = dueDate;
            Status = status;
        }

        private string _task;
        private int _difficulty;
        private DateTime _dueDate;
        private StatusEnum _status;

        string Task
        {
            get 
            { 
                return _task; 
            }
            set
            {
                if (value.Length < 1 || value.Length > 100 || !Regex.Match(value, @"[A-Za-z0-9\\.\\,\\s_()/\-]+").Success || value.Contains(';'))
                {
                    throw new InvalidParameterException("Task must be 1-100 characters, " +
                        "made up of uppercase and lowercase letters, digits, and space, _-(),./\\ characters");
                }
                _task = value;
            }
        }
        int Difficulty
        {
            get
            {
                return _difficulty;
            }
            set
            {
                if (value < 0 || value > 5)
                {
                    throw new ArgumentException("Difficulty must be between 0 and 5");
                }
                _difficulty = value;
            }
        }
        
        public string DueDate
        {
            get
            {
                return _dueDate.ToString();
            }
            set
            {
                DateTime.TryParse(value, out _dueDate);
            }
        }
            
        public StatusEnum Status
        { 
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public enum StatusEnum { Peding, Done, Delegated }
    }
}
