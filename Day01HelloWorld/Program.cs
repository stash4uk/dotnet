﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("What is your name?");
            string name = Console.ReadLine();
            Console.Write("How old are you?");
            int age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Hello " + name + ", you are " + age + " y/o, nice to meet you.");
            // Console.WriteLine("Hello " + name + ", you are " + age + " y/o, nice to meet you.");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }   
    }
}
