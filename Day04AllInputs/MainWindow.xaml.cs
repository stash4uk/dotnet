﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day04AllInputs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_RegisterMeClick(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string sAge = "";
            if ((bool)below18.IsChecked)
            {
                sAge = below18.Content.ToString();
            }
            else if ((bool)_18_35.IsChecked)
            {
                sAge = _18_35.Content.ToString();
            }
            else if ((bool)_36up.IsChecked)
            {
                sAge = _36up.Content.ToString();
            }

            List<string> pList = new List<string>();
            List<string> output = new List<string>();

            if ((bool)Cat.IsChecked)
            {
                pList.Add("Cat");
            }
            if ((bool)Dog.IsChecked)
            {
                pList.Add("Dog");
            }
            if ((bool)Other.IsChecked)
            {
                pList.Add("Other");
            }

            string pets = string.Join(",", pList);

            ComboBoxItem item = (ComboBoxItem)cbContinent.SelectedValue;
            var content = (string)item.Content;

            int prefTempC = (int)sliderTempC.Value;

            string data = string.Format("{0};{1};{2};{3};{4}", name, sAge, pets, content, prefTempC);
            
            output.Add(data);

            File.AppendAllLines(@"..\..\people.txt", output);

            tbName.Clear();
            Cat.IsChecked = false;
            Dog.IsChecked = false;
            Other.IsChecked = false;
            below18.IsChecked = true;
            sliderTempC.Value = 15;

            MessageBox.Show("Record added.");

            //this.Close();
        }

            
    }
}
