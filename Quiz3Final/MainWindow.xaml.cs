﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz3Final
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        SaveFileDialog sfd = new SaveFileDialog();

        private void MenuItemAdd_Click(object sender, RoutedEventArgs e)
        {
            dlgAddEdit addEditDialog = new dlgAddEdit();
            addEditDialog.Owner = this;
            bool? result = addEditDialog.ShowDialog();
        }

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void MenuItemSaveSelected_Click(object sender, RoutedEventArgs e)
        {
            sfd.FileName = "";
            sfd.Filter = "Text Documents *.txt|All Files *.*";
            sfd.DefaultExt = ".txt";
            if (sfd.ShowDialog() == true)
            {
                
            }
        }
    }
}
