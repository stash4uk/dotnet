﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Final
{
    public class Flight
    {
        public Flight(long id, DateTime onDay, string fromCode, string toCode, Type type)
        {
            Id = Id;
            OnDay = onDay;
            FromCode = fromCode;
            ToCode = toCode;
            
        }

        private long _id;
        private DateTime _onDay;
        private string _fromCode;
        private string _toCode;
        private Type _type;

        long Id { get; set; }
        DateTime OnDay { get; set; }
        string FromCode { get; set; }
        string ToCode { get; set; }
        

        public enum Type { Domestic, International, Private }
    }
}
