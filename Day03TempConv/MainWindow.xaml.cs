﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day03TempConv
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public double GetInput()
        {
            string strInTemp = tbInput.Text;
            double inTemp;
            if (!double.TryParse(strInTemp, out inTemp))
            {
                MessageBox.Show("Parsing error");
            }
            return inTemp;
        }

        public void CalcCelsius()
        {
            double outTempC = 0;
            GetInput();
            if ((bool)rbCelsGroupIn.IsChecked)
            {
                outTempC = GetInput();
            }
            if ((bool)rbFahrGroupIn.IsChecked)
            {
                outTempC = (GetInput() - 32) * 5 / 9;
            }
            if ((bool)rbKelvGroupIn.IsChecked)
            {
                outTempC = GetInput() - 273.15;
            }

            tbOutput.Text = outTempC.ToString("#.##");
        }

        public void CalcFahrenheit()
        {
            double outTempF = 0;
            GetInput();
            if ((bool)rbCelsGroupIn.IsChecked)
            {
                outTempF = GetInput() * 9 / 5 + 32;
            }
            if ((bool)rbFahrGroupIn.IsChecked)
            {
                outTempF = GetInput();
            }
            if ((bool)rbKelvGroupIn.IsChecked)
            {
                outTempF = (GetInput() - 273.15) * 9 / 5 + 32;
            }

            tbOutput.Text = outTempF.ToString("#.##");
        }

        public void CalcKelvin()
        {
            double outTempK = 0;
            GetInput();
            if ((bool)rbCelsGroupIn.IsChecked)
            {
                outTempK = GetInput() + 273.15;
            }
            if ((bool)rbFahrGroupIn.IsChecked)
            {
                outTempK = (GetInput() - 32) * 5 / 9 + 273.15;
            }
            if ((bool)rbKelvGroupIn.IsChecked)
            {
                outTempK = GetInput();
            }

            tbOutput.Text = outTempK.ToString("#.##");
        }

        private void rbCelsGroupOut_Click(object sender, RoutedEventArgs e)
        {
            CalcCelsius();
        }

        private void rbFahrGroupOut_Click(object sender, RoutedEventArgs e)
        {
            CalcFahrenheit();
        }

        private void rbKelvGroupOut_Click(object sender, RoutedEventArgs e)
        {
            CalcKelvin();
        }

        private void tbInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            if ((bool)rbCelsGroupIn.IsChecked)
            {
                rbCelsGroupIn.IsChecked = false;
            }
            if ((bool)rbFahrGroupIn.IsChecked)
            {
                rbFahrGroupIn.IsChecked = false;
            }
            if ((bool)rbKelvGroupIn.IsChecked)
            {
                rbKelvGroupIn.IsChecked = false;
            }
            if ((bool)rbCelsGroupOut.IsChecked)
            {
                rbCelsGroupOut.IsChecked = false;
            }
            if ((bool)rbFahrGroupOut.IsChecked)
            {
                rbFahrGroupOut.IsChecked = false;
            }
            if ((bool)rbKelvGroupOut.IsChecked)
            {
                rbKelvGroupOut.IsChecked = false;
            }
            tbOutput.Text = "";
        }
    }
}
