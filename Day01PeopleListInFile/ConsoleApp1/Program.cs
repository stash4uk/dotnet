﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static String[] nameList = new String[5];
        static void Main(string[] args)
        {
            try
            {
                for (int i = 0; i < nameList.Length; i++)
                {
                    Console.Write("Enter a name");
                    nameList[i] = Console.ReadLine();
                }
                Console.Write("Enter search string");
                string search = Console.ReadLine();
                foreach (String n in nameList)
                {
                    if (n.Contains(search))
                    {
                        Console.WriteLine("{0} contains {1} string", n, search);
                    }
                }

                String longest = nameList[0];
                foreach (String n in nameList)
                {
                    if (longest.Length < n.Length)
                    {
                        longest = n;
                    }
                    Console.WriteLine("The longest is " + longest);
                }
            }
            finally
            {
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }

        }
    }
}
