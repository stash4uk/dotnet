﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day01PeopleListInFile
{
    public class Person
    {
        public Person(string name, int age, string city)
        {
            Name = name;
            Age = age;
            City = city;
        }
        
        private string _name;
        private int _age;
        private string _city;
        
        public string Name // Name 2-100 characters long, not containing semicolons
        {
            get
            {
                return _name;
            }
            set
            {
                int lengthName = value.Length;
                if (lengthName < 2 || lengthName > 100 || value.Contains(";"))
                {
                    throw new ArgumentException("Name must be between 2 and 100 characters long");
                }
                _name = value;
            }
        }
        

        public int Age // Age 0-150
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentException("Age must be between 0 and 150");
                }
                _age = value;
            }
        }
        public string City // City 2-100 characters long, not containing semicolons
        {
            get
            {
                return _city;
            }
            set
            {
                int lengthCity = value.Length;
                if (lengthCity < 2 || lengthCity > 100 || value.Contains(";"))
                {
                    throw new ArgumentException("City must be between 2 and 100 characters long");
                }
                _city = value;
            }
        }

        public override string ToString()
        {
            return string.Format("{0} is {1} from {2}", Name, Age, City);
        }

        
    }
    class Program
    {
        public const string FilePath = @"..\..\people.txt";
        static void ReadAllPeopleFromFile()
        {
            
                List<string> lines = File.ReadAllLines(FilePath).ToList();

                foreach (var line in lines)
                {
                    try
                    {
                        string[] records = line.Split(',');
                        String name = records[0];
                        int age = Convert.ToInt32(records[1]);
                        String city = records[2];
                        Person p = new Person(name, age, city);
                        people.Add(p);

                        Console.WriteLine(p.ToString());
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine("Error reading from file: " + ex.Message);
                        continue;
                    }
                    catch (FormatException ex)
                    {
                        Console.WriteLine("Error reading from file: " + ex.Message);
                        continue;
                    }
                    catch (FileNotFoundException ex)
                    {
                        Console.WriteLine("Error accessing file: " + ex.Message);
                        continue;
                    }
                }
        }

        private static int GetMenuChoice()
        {
            while (true)
            {
                Console.Write(
    @"1. Add person info
2. List persons info
3. Find a person by name
4. Find all persons younger than age
0. Exit
Enter your choice: "
                    );
                string choiceStr = Console.ReadLine();
                int choice;
                if (!int.TryParse(choiceStr, out choice) || choice < 0 || choice > 4)
                {
                    Console.WriteLine("Error: Enter an integer value between 0 and 4");
                    continue;
                }
                return choice;
            }
        }

        static void AddPersonInfo()
        {

        Console.WriteLine("Adding a person.");
        Console.Write("Enter name: ");
        string name = Console.ReadLine();
        Console.Write("Enter age: ");
        string ageStr = Console.ReadLine();
        int age;
        if (!int.TryParse(ageStr, out age))
        {
            Console.WriteLine("Error: Enter an integer value for age.");
                return;
        }
        Console.Write("Enter city: ");
        string city = Console.ReadLine();

            try
            {
                Person psn = new Person(name, age, city);
                people.Add(psn);

                Console.WriteLine("Person added.");
            } catch (ArgumentException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            
        }
        static void ListAllPersonsInfo()
        {
                //Console.WriteLine(string.Join("\n", people));

            foreach (Person p in people)
            {
                Console.WriteLine(p.ToString());
            }    
        }
        static void FindPersonByName()
        {
            Console.Write("Enter partial person name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Matches found: ");
            foreach (Person p in people)
            {
                if (p.Name.ToLower().Contains(name.ToLower()))
                    Console.WriteLine(p);
            }
        }
        static void FindPersonYoungerThan()
        {
            Console.Write("Enter maximum age: ");
            string ageStr = Console.ReadLine();
            int age;
            if (!int.TryParse(ageStr, out age))
            {
                Console.WriteLine("Error: Enter an integer value for age.");
                return;
            }
            Console.WriteLine("Matches found: ");
            foreach (Person p in people)
            {
                if (p.Age < age)
                {
                    Console.WriteLine(p);
                }
            }
        }

        static void SaveAllPeopleToFile()
        {
            foreach (Person person in people)
            {
                output.Add($"{person.Name},{person.Age},{person.City}");
            }
            File.WriteAllLines(FilePath, output);
        }

        static List<Person> people = new List<Person>();
        static List<string> output = new List<string>();

        static void Main(string[] args)
        {
            ReadAllPeopleFromFile();
            Console.WriteLine();
            while (true)
            {
                int choice = GetMenuChoice();
                switch (choice)
                {
                    case 1:
                        AddPersonInfo();
                        break;
                    case 2:
                        ListAllPersonsInfo();
                        break;
                    case 3:
                        FindPersonByName();
                        break;
                    case 4:
                        FindPersonYoungerThan();
                        break;
                    case 0:
                        SaveAllPeopleToFile();
                        Console.WriteLine("Data saved. Program will stop.");
                        return;
                    default:
                        Console.WriteLine("Error: Invalid choice.");
                        break;

                }

                Console.ReadLine();
            }
        }
    }
}

